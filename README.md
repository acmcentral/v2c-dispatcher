# V2C Dispatcher

*Copyright (c) 2020 Edmond ACM Chapter. All rights reserved.*

## Build

This project requires Java 11 and has been successfully built and executed on FreeBSD, Windows, and several GNU/Linux flavors.
This project can be tested and compiled with the following command.

`gradlew clean shadowJar`

## Execution

To run it, just do `java -jar build/libs/v2c-dispatcher.jar`.

You can optionally specify some command-line arguments.

|Short Param|Long Param|Description         |Default|
|:----------|:---------|:-------------------|:------|
|-d         |--debug   |Enable debug logging|false  |
|-p         |--port    |The port number     |2585   |

## Contributions

We could definitely use your help here-- if you'd like to contribute, please read our [CONTRIBUTING](CONTRIBUTING.md) notice.

## License

**This repository is subject to the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).**
