package edu.uco.cs.v2c.dispatcher.log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONObject;

/**
 * Represents a log entry.
 * 
 * @author Caleb L. Power
 */
public class LogEntry extends JSONObject {

  /**
   * Denotes the importance of the log entry in question.
   * 
   * @author Caleb L. Power
   */
  public static enum LogLevel {
    /**
     * Denotes that the log entry in question is of a debugging nature.
     */
    DEBUG,
    
    /**
     * Denotes that the log entry in question is of an informational nature. 
     */
    INFO,
    
    /**
     * Denotes that the log entry in question is of an erroneous nature.
     */
    ERROR
  }
  
  private static final Calendar CALENDAR = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
  private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
  private static final String STRING_FORMAT = "[%1$s: %2$s] %3$s - %4$s";
  private static final String TYPE_KEY = "type";
  private static final String LABEL_KEY = "label";
  private static final String MESSAGE_KEY = "message";
  private static final String TIMESTAMP_KEY = "timestamp";
  
  /**
   * Formats a timestamp to something vaguely human-readable.
   * 
   * @param timestamp the timestamp
   * @return a String representing the log entry's time of creation.
   */
  public static synchronized String format(long timestamp) {
    CALENDAR.setTimeInMillis(timestamp);
    return DATE_FORMAT.format(CALENDAR.getTime());
  }
  
  /**
   * Instantiates the log entry.
   * 
   * @param type the log type
   * @param label the component
   * @param message the details of the message
   * @param timestamp the creation time of the message
   */
  public LogEntry(LogLevel type, String label, String message, long timestamp) {
    put(TYPE_KEY, type.name());
    put(LABEL_KEY, label);
    put(MESSAGE_KEY, message);
    put(TIMESTAMP_KEY, format(timestamp));
  }
  
  @Override public String toString() {
    return String.format(STRING_FORMAT,
        getString(TIMESTAMP_KEY),
        getString(TYPE_KEY),
        getString(LABEL_KEY),
        getString(MESSAGE_KEY));
  }
  
}
