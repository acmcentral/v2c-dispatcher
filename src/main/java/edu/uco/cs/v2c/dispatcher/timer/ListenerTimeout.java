/*
 * Copyright (c) 2020 Edmond ACM Chapter. Original code copyright (c) 2020
 * Caleb L. Power, Everistus Akpabio, Rashed Alrashed, Nicholas Clemmons,
 * Jonathan Craig, James Cole Riggall, and Glen Mathew. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.uco.cs.v2c.dispatcher.timer;

import org.eclipse.jetty.websocket.api.Session;

import edu.uco.cs.v2c.dispatcher.V2CDispatcher;
import edu.uco.cs.v2c.dispatcher.net.websocket.WebSocketHandler;

/**
 * Disconnects a session if it fails to register a listener within the
 * allocated time allotted.
 * 
 * @author Jon Craig, Caleb L. Power
 */
public class ListenerTimeout implements Runnable {
  
  private static final String LOG_LABEL = "WEBSOCKET HANDLER - LISTENER TIMEOUT";
  
  private Session session = null;
  
  /**
   * Instantiates the listener timeout.
   * 
   * @param session the session in question
   */
  public ListenerTimeout(Session session) {
    this.session = session;
  }
  
  @Override public void run() {
    try {
      if(!WebSocketHandler.getModuleMap().containsKey(session)) {
        V2CDispatcher.getLogger().logError(LOG_LABEL,
            String.format("%1$s:%2$d listener not registered in time.",
                session.getRemoteAddress().getHostString(),
                session.getRemoteAddress().getPort()));
        session.close(1, "Registration timed out.");
      }
    } catch(NullPointerException e) {
      V2CDispatcher.getLogger().logError(LOG_LABEL, "Session closed before timer elapsed.");
    }
  }
  
}
