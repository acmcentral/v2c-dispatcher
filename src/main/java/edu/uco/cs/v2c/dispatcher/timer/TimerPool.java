/*
 * Copyright (c) 2020 Edmond ACM Chapter. Original code copyright (c) 2020
 * Caleb L. Power, Everistus Akpabio, Rashed Alrashed, Nicholas Clemmons,
 * Jonathan Craig, James Cole Riggall, and Glen Mathew. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.uco.cs.v2c.dispatcher.timer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Executes a designated set of actions at various times. If two actions are
 * set to run at the same time, they are set sequentially in FIFO fashion.
 * 
 * @author Caleb L. Power
 */
public class TimerPool implements Runnable {
  
  private Thread thread = null;
  private Map<Runnable, Long[]> actions = new HashMap<>();
  
  @Override public void run() {
    try {
      for(;;) {
        Thread.sleep(1000L);
        long now = System.currentTimeMillis();
        synchronized(actions) {
          Set<Runnable> toRemove = new HashSet<>();
          for(Entry<Runnable, Long[]> action : actions.entrySet())
            if(action.getValue()[0] + action.getValue()[1] < now) {
              if(action.getValue()[2] == 0)
                toRemove.add(action.getKey());
              else action.getValue()[0] = System.currentTimeMillis();
              action.getKey().run();
            }
          for(Runnable action : toRemove)
            actions.remove(action);
        }
      }
    } catch(InterruptedException e) { }
  }
  
  /**
   * Sets a particular runnable to run repeatedly at a delay.
   * If the action is already known, resets the time and delay appropriately.
   * 
   * @param runnable the runnable
   * @param delay the delay, in seconds
   * @param repeat {@true} true iff the runnable should repeat
   */
  public synchronized void setAction(Runnable runnable, int delay, boolean repeat) {
    Long[] times = new Long[] { System.currentTimeMillis(), delay * 1000L, repeat ? 1L : 0L };
    
    synchronized(actions) {
      if(actions.containsKey(runnable))
        actions.replace(runnable, times);
      else actions.put(runnable, times);
    }
    
    if(thread == null) {
      thread = new Thread(this);
      thread.setDaemon(true);
      thread.start();
    }
  }
  
  /**
   * Resets the timer for the action, if it's known.
   * 
   * @param runnable the action in question
   */
  public synchronized void resetAction(Runnable runnable) {
    synchronized(actions) {
      if(actions.containsKey(runnable))
        actions.get(runnable)[0] = System.currentTimeMillis();
    }
  }
  
  /**
   * Removes the action from the pool if it exists.
   * 
   * @param runnable the action
   */
  public synchronized void removeAction(Runnable runnable) {
    synchronized(actions) {
      if(actions.containsKey(runnable))
        actions.remove(runnable);
    }
  }
  
}
