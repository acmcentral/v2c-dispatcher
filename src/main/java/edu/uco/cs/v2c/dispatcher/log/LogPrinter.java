/*
 * Copyright (c) 2020 Edmond ACM Chapter. ALl rights reserved. Original code
 * copyright (c) 2019 Axonibyte Innovations, LLC. All rights reserved.
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.uco.cs.v2c.dispatcher.log;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.axonibyte.bonemesh.listener.LogListener;

import edu.uco.cs.v2c.dispatcher.log.LogEntry.LogLevel;

/**
 * Log manager.
 * 
 * @author Caleb L. Power
 */
public class LogPrinter implements LogListener {
  
  private static final int MAX_LOG_LENGTH = 1000;
  
  private boolean debug = false;
  private BoneMeshLogCatcher boneMeshLogCatcher = null;
  private List<LogEntry> ledger = new CopyOnWriteArrayList<>();
  
  /**
   * Instantiates the log printer.
   * 
   * @param debug {@true} to enable debug logs
   */
  public LogPrinter(boolean debug) {
    this.debug = debug;
    this.boneMeshLogCatcher = new BoneMeshLogCatcher();
  }

  /**
   * {@inheritDoc}
   */
  @Override public void onDebug(String label, String message, long timestamp) {
    if(debug) log(LogLevel.DEBUG, label, message, timestamp);
  }
  
  /**
   * Acts on a debugging message.
   * 
   * @param label the message tag
   * @param message the message
   * @param args the formatting arguments
   */
  public void onDebug(String label, String message, Object... args) {
    onDebug(label, String.format(message + '\n', args), System.currentTimeMillis());
  }

  /**
   * {@inheritDoc}
   */
  @Override public void onInfo(String label, String message, long timestamp) {
    log(LogLevel.INFO, label, message, timestamp);
  }
  
  /**
   * Acts on an informational message.
   * 
   * @param label the message tag
   * @param message the message
   * @param args the formatting arguments
   */
  public void onInfo(String label, String message, Object... args) {
    onInfo(label, String.format(message + '\n', args), System.currentTimeMillis());
  }

  /**
   * {@inheritDoc}
   */
  @Override public void onError(String label, String message, long timestamp) {
    log(LogLevel.ERROR, label, message, timestamp);
  }
  
  /**
   * Acts on an error message.
   * 
   * @param label the message tag
   * @param message the message
   * @param args the formatting arguments
   */
  public void onError(String label, String message, Object... args) {
    onError(label, String.format(message + '\n', args), System.currentTimeMillis());
  }
  
  private void log(LogLevel type, String label, String message, long timestamp) {
    // log logger messages to STD out and push to JSONArray log
    LogEntry entry = new LogEntry(type, label, message, timestamp);
    System.out.println(entry.toString());
    if(ledger.size() >= MAX_LOG_LENGTH) ledger.remove(0);
    ledger.add(entry);
  }
  
  /**
   * Retrieves the log ledger.
   * 
   * @return the ledger, a JSONArray of log objects
   */
  public List<LogEntry> getLedger(){
	  return ledger;
  }
  
  /**
   * Retrieves the BoneMesh-specific logger.
   * 
   * @return the BoneMesh logger
   */
  public BoneMeshLogCatcher getBoneMeshLogCatcher() {
    return boneMeshLogCatcher;
  }
  
  private class BoneMeshLogCatcher implements LogListener {
    
    /**
     * {@inheritDoc}
     */
    @Override public void onDebug(String label, String message, long timestamp) {
      if(debug) LogPrinter.this.onDebug("<BONEMESH DEBUG> " + label, message, timestamp);
    }

    /**
     * {@inheritDoc}
     */
    @Override public void onInfo(String label, String message, long timestamp) {
      LogPrinter.this.onDebug("<BONEMESH INFO> " + label, message, timestamp);
    }

    /**
     * {@inheritDoc}
     */
    @Override public void onError(String label, String message, long timestamp) {
      LogPrinter.this.onDebug("<BONEMESH ERROR> " + label, message, timestamp);
    }
    
  }

}
