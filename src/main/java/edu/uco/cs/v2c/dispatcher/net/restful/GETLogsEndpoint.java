/*
 * Copyright (c) 2020 Edmond ACM Chapter. Original code copyright (c) 2020
 * Caleb L. Power, Everistus Akpabio, Rashed Alrashed, Nicholas Clemmons,
 * Jonathan Craig, James Cole Riggall, and Glen Mathew. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.uco.cs.v2c.dispatcher.net.restful;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uco.cs.v2c.dispatcher.V2CDispatcher;
import edu.uco.cs.v2c.dispatcher.net.APIVersion;
import spark.Request;
import spark.Response;

/**
 * Retrieves the most recent logs.
 * 
 * @author Jon Craig, Caleb L. Power
 */
public class GETLogsEndpoint extends Endpoint {
	
  private static final int DEFAULT_MAX_LOG_LENGTH = 100;
  private static final String MAX_PARAM = "max";
  
	/**
	 * Instantiates the endpoint.
	 */
	public GETLogsEndpoint() {
		super("/log",APIVersion.VERSION_1,HTTPMethod.GET);
	}
	
	@Override public JSONObject doEndpointTask(Request req, Response res) throws EndpointException {
	  int max = DEFAULT_MAX_LOG_LENGTH;
	  
	  try {
	    if(req.queryParams().contains(MAX_PARAM))
	      max = Integer.parseInt(req.queryParams(MAX_PARAM));
	  } catch(NumberFormatException e) {
	    throw new EndpointException(req, "Syntax error.", 400, e);
	  }
	  
	  JSONArray logs = new JSONArray();
	  for(int i = 1; i <= max && i <= V2CDispatcher.getLogLedger().size(); i++)
	    logs.put(V2CDispatcher.getLogLedger().get(
	        V2CDispatcher.getLogLedger().size() - i));
	  
	  return new JSONObject()
	      .put("status", "ok")
	      .put("info", "Retrieved logs.")
	      .put("logs", logs);
	}
}
